import java.util.Scanner;
import java.util.Random;
public class Wordle{
	
	public static boolean gamestate = false;
	
	public static void runGame(){
		int lives = 6;
		String WORD = generateWord();
		Scanner uInput = new Scanner(System.in);
		String guess = "";
		
		//Creates the colors array
		String[] colors = new String[WORD.length()];
		
		while(lives>0 && !gamestate){
			//obtain the guess
			System.out.println("What is your guess?");
			guess = uInput.nextLine().toUpperCase();
			
			
			//Makes sure that the guess is five letters long
			if (guess.length() != 5){
				System.out.println("Not a valid word");
			}else{
				colors = compareWords(WORD, guess);
				presentResult(colors, guess);
			}
			lives--;
		}
		//checks if the user ran out of guesses or won
		if(gamestate){
			System.out.println("You Win!!!");
		}else{
			System.out.println("You lose, the word was " + WORD);
		}
	}
	
	//returns a random word from the list of previous wordle words
	public static String generateWord(){
		String[] wordleList = {"ABACK","ABASE","ABATE","ABBEY","ABOUT","ABOVE","ABYSS","ACRID","ACTOR","ACUTE","ADMIT","ADOBE","ADOPT","ADORE","ADULT","AGAIN","AGAPE","AGATE","AGENT","AGILE","AGLOW","AGONY","AGREE","AHEAD","ALBUM","ALIEN","ALIKE","ALLOW","ALOFT","ALONE","ALOUD","ALPHA","ALTAR","ALTER","AMBER","AMISS","AMPLE","ANGEL","ANGER","ANGRY","ANODE","ANTIC","AORTA","APHID","APPLE","APPLY","APRON","APTLY","ARBOR","ARDOR","ARGUE","AROMA","ASIDE","ASKEW","ASSET","ATOLL","ATONE","AUDIO","AUDIT","AVAIL","AVERT","AWAIT","AWAKE","AWFUL","AXIOM","AZURE","BADGE","BADLY","BAGEL","BAKER","BALSA","BANAL","BARGE","BASIC","BATHE","BATON","BATTY","BAYOU","BEACH","BEADY","BEAST","BEEFY","BEGET","BEGIN","BEING","BELCH","BELIE","BELLY","BELOW","BENCH","BERET","BERTH","BESET","BINGE","BIOME","BIRCH","BIRTH","BLACK","BLAME","BLAND","BLEAK","BLEED","BLEEP","BLOKE","BLOWN","BLUFF","BLURB","BLURT","BLUSH","BOOBY","BOOST","BOOZE","BOOZY","BORAX","BOUGH","BRAID","BRAKE","BRASH","BRAVE","BREAD","BREAK","BRIAR","BRIBE","BRIDE","BRINE","BRING","BRINK","BRISK","BROKE","BROOK","BROOM","BRUSH","BUGGY","BULLY","BUNCH","BURLY","CACAO","CACHE","CANDY","CANNY","CANOE","CAPER","CARAT","CARGO","CARRY","CAROL","CATCH","CATER","CAULK","CAUSE","CEDAR","CHAFE","CHAMP","CHANT","CHARD","CHARM","CHART","CHEAT","CHEEK","CHEST","CHIEF","CHILL","CHIME","CHOIR","CHOKE","CHORD","CHUNK","CHUTE","CIDER","CIGAR","CINCH","CIRCA","CIVIC","CLASS","CLEAN","CLEAR","CLERK","CLICK","CLING","CLOCK","CLOSE","CLOTH","CLOWN","CLUCK","COACH","COAST","COCOA","COLON","COMET","COMMA","CONDO","CONIC","CORNY","COULD","COUNT","COVET","COWER","COYLY","CRAMP","CRANE","CRANK","CRASS","CRATE","CRAVE","CRAZE","CRAZY","CREAK","CREDO","CREPT","CRIME","CRIMP","CROAK","CRONE","CROSS","CRUMB","CRUST","CURLY","CYNIC","DADDY","DANCE","DANDY","DEATH","DEBUG","DELTA","DELVE","DENIM","DEPOT","DEPTH","DIGIT","DINER","DISCO","DITTO","DODGE","DONOR","DONUT","DOUBT","DOWRY","DOZEN","DRAIN","DREAM","DRINK","DRIVE","DROLL","DROOP","DUCHY","DUTCH","DUVET","DWARF","DWELL","DWELT","EARTH","EGRET","EJECT","ELDER","ELOPE","ELUDE","EMAIL","EMPTY","ENEMA","ENJOY","ENNUI","ENTER","EPOCH","EPOXY","EQUAL","ERODE","ERROR","ESSAY","ETHIC","ETHOS","EVADE","EVERY","EXACT","EXCEL","EXERT","EXIST","EXTRA","EXULT","FARCE","FAULT","FAVOR","FEAST","FEIGN","FERRY","FEWER","FIELD","FIEND","FIFTY","FINER","FIRST","FISHY","FIXER","FJORD","FLAIL","FLAIR","FLANK","FLARE","FLASK","FLESH","FLICK","FLING","FLIRT","FLOAT","FLOCK","FLOOD","FLOOR","FLORA","FLOSS","FLOUT","FLUFF","FLUME","FLYER","FOCAL","FOCUS","FOGGY","FOLLY","FORAY","FORGE","FORGO","FORTH","FOUND","FOYER","FRAME","FRANK","FRESH","FROCK","FRONT","FROST","FROTH","FROZE","FUNGI","GAMER","GAMMA","GAUDY","GAUZE","GAWKY","GECKO","GHOUL","GIANT","GIDDY","GIRTH","GIVEN","GLASS","GLAZE","GLEAN","GLOAT","GLOOM","GLORY","GLOVE","GLYPH","GNASH","GOLEM","GONER","GOOSE","GORGE","GOUGE","GRADE","GRAIL","GRAND","GRAPH","GRATE","GREAT","GREEN","GREET","GRIEF","GRIME","GRIMY","GRIPE","GROIN","GROUP","GROUT","GROVE","GROWL","GRUEL","GUANO","GUARD","GUEST","GUIDE","GUILD","GULLY","GUPPY","HAIRY","HAPPY","HATCH","HATER","HAVOC","HEADY","HEART","HEATH","HEIST","HELIX","HELLO","HERON","HINGE","HOARD","HOBBY","HOMER","HORDE","HORSE","HOTEL","HOUND","HOWDY","HUMAN","HUMID","HUMOR","HUMPH","HUNKY","HURRY","HUTCH","HYPER","IGLOO","IMPEL","INANE","INDEX","INEPT","INERT","INFER","INPUT","INTER","IONIC","IRATE","IRONY","ISLET","ITCHY","IVORY","JAUNT","JAZZY","JOKER","JOUST","JUDGE","KARMA","KAYAK","KAZOO","KEBAB","KHAKI","KIOSK","KNEEL","KNELT","KNOCK","KNOLL","KOALA","LABEL","LABOR","LAPEL","LAPSE","LARVA","LATTE","LAYER","LEAFY","LEAKY","LEAPT","LEASH","LEAVE","LEDGE","LEERY","LEMON","LIBEL","LIGHT","LILAC","LIMIT","LINEN","LIVER","LOCUS","LOFTY","LOGIC","LOOPY","LOSER","LOVER","LOWLY","LOYAL","LUCKY","LUNAR","LUSTY","LYING","MADAM","MAGIC","MAGMA","MAIZE","MAJOR","MANIA","MANLY","MANOR","MAPLE","MARCH","MARRY","MARSH","MASON","MASSE","MATEY","MAXIM","MAYBE","MEALY","MEANT","MEDAL","MERCY","MERIT","MERRY","METAL","METRO","MIDGE","MIDST","MIMIC","MINCE","MODEL","MOIST","MOLAR","MONEY","MONTH","MOOSE","MOSSY","MOTOR","MOTTO","MOULT","MOUNT","MOURN","MOUSE","MOVIE","MUCKY","MUMMY","MUSIC","NAIVE","NANNY","NASTY","NATAL","NAVAL","NEEDY","NIGHT","NINJA","NINTH","NOBLE","NOISE","NYMPH","OCCUR","OCEAN","OFFAL","OLDER","OLIVE","ONION","ONSET","OPERA","OTHER","OUGHT","OUTDO","OXIDE","PANEL","PANIC","PAPER","PARER","PARRY","PARTY","PATTY","PAUSE","PEACE","PEACH","PERCH","PERKY","PHASE","PHONY","PHOTO","PIANO","PICKY","PIETY","PILOT","PINEY","PINKY","PINTO","PIQUE","PITHY","PIXEL","PIXIE","PLANK","PLANT","PLATE","PLAZA","PLEAT","PLUCK","PLUNK","POINT","POISE","POKER","POLKA","POLYP","POUND","POWER","PRICK","PRIDE","PRIME","PRIMO","PRINT","PRIZE","PROBE","PROVE","PROXY","PULPY","PURGE","QUALM","QUART","QUEEN","QUERY","QUEST","QUEUE","QUICK","QUIET","QUIRK","QUOTE","RADIO","RAINY","RAMEN","RANCH","RANGE","RATIO","RAYON","REACT","REBUS","REBUT","RECAP","REGAL","RENEW","REPAY","RETCH","RETRO","RETRY","REVEL","RHINO","RHYME","RIGHT","RIPER","RIVAL","ROBIN","ROBOT","ROCKY","RODEO","ROGUE","ROOMY","ROUGE","ROUND","ROUSE","ROYAL","RUDDY","RUDER","RUPEE","RUSTY","SAINT","SALAD","SALSA","SASSY","SAUTE","SCALD","SCARE","SCARF","SCOLD","SCOPE","SCORN","SCOUR","SCOUT","SCRAP","SCRUB","SEDAN","SEEDY","SERVE","SEVER","SHAKE","SHALL","SHAME","SHARD","SHAWL","SHINE","SHIRE","SHIRK","SHORN","SHOWN","SHOWY","SHRUB","SHRUG","SHYLY","SIEGE","SIGHT","SISSY","SKILL","SKIMP","SKIRT","SKUNK","SLATE","SLEEK","SLOSH","SLOTH","SLUMP","SLUNG","SMART","SMASH","SMEAR","SMELT","SMILE","SMIRK","SMITE","SNACK","SNAFU","SNAIL","SNAKY","SNARE","SNARL","SNEAK","SNOUT","SOGGY","SOLAR","SOLID","SOLVE","SONIC","SOUND","SOWER","SPACE","SPADE","SPELL","SPEND","SPICE","SPICY","SPIEL","SPIKE","SPILL","SPIRE","SPLAT","SPOKE","SPRAY","SPURT","SQUAD","SQUAT","STAFF","STAGE","STAID","STAIR","STALE","STAND","START","STEAD","STEED","STEIN","STICK","STING","STINK","STOCK","STOMP","STONE","STOOL","STORE","STORY","STOUT","STOVE","STRAP","STRAW","STUDY","STYLE","SUGAR","SULKY","SURER","SURLY","SUSHI","SWEAT","SWEEP","SWEET","SWILL","SWINE","SWIRL","SYRUP","TACIT","TANGY","TAPER","TAPIR","TARDY","TASTE","TASTY","TAUNT","TAWNY","TEASE","TEMPO","TENTH","TEPID","THEIR","THEME","THERE","THIEF","THINK","THIRD","THORN","THOSE","THROW","THUMB","THUMP","THYME","TIARA","TIBIA","TIGER","TILDE","TIPSY","TODAY","TONIC","TOPAZ","TORSO","TOTEM","TOUGH","TOXIC","TRACE","TRACT","TRADE","TRAIN","TRAIT","TRASH","TRAWL","TREAT","TREND","TRIAD","TRICE","TRITE","TROLL","TROPE","TROVE","TRUSS","TRUST","TRUTH","TRYST","TWANG","TWEED","TWICE","TWINE","ULCER","ULTRA","UNCLE","UNDER","UNDUE","UNFED","UNFIT","UNIFY","UNITE","UNLIT","UNMET","UNTIE","UNTIL","UNZIP","UPSET","USAGE","USHER","USING","USUAL","USURP","UTTER","VAGUE","VALET","VALID","VENOM","VERVE","VIGOR","VIOLA","VIRAL","VITAL","VIVID","VODKA","VOICE","VOTER","VOUCH","WACKY","WALTZ","WASTE","WATCH","WEARY","WEDGE","WHACK","WHALE","WHEEL","WHELP","WHERE","WHIFF","WHILE","WHINE","WHIRL","WHISK","WHOOP","WINCE","WINDY","WOKEN","WOOER","WORDY","WORLD","WORRY","WORSE","WOVEN","WRATH","WRITE","WRONG","WROTE","WRUNG","YACHT","YEARN","YIELD","YOUTH","ZESTY"};
		Random rand = new Random();
		int x = rand.nextInt(wordleList.length);
		return wordleList[x];
	}
	
	
	//Cannot return array here, so returns string for each individual character (requires it to be called in a forloop)
	public static String letterInSlot(String WORD, String guess, int I){
		if(WORD.charAt(I) == guess.charAt(I)){
			return "Green";
		}
		return "";
	}
	
	//Checks if letter given is in the word (uses tempWord to avoid coloring doubles if there is only one in the word)
	public static boolean letterInWord(char letter, char[] tempWord){
		for(int i = 0; i<tempWord.length; i++){
			if(tempWord[i] == letter){
				position = i;
				return true;
			}
		}
		return false;
	}
	
	//to send the character index from one function to another (letterInWord to compareWords)
	public static int position = 0;
	
	public static String[] compareWords(String WORD, String guess){
		String[] tempColors = new String[WORD.length()];
		//Temporary char array variables. This is done for the use of doubles
		char[] tempGuess = new char[guess.length()];
		char[] tempWord = new char[WORD.length()];
		for(int i = 0; i<tempWord.length;i++){
			tempGuess[i] = guess.charAt(i);
			tempWord[i] = WORD.charAt(i);
		}
		
		//Finds all the exact letters (GREEN)
		for(int i =0; i<tempColors.length; i++){
			tempColors[i] = letterInSlot(WORD, guess, i);
		}
		
		//removes the correct letters from the temps (once again for the doubles)
		for(int i = 0; i<tempColors.length;i++){
			if(tempColors[i].equals("Green")){
				tempGuess[i] = '0';
				tempWord[i] = '0';
			}
		}
		
		//Checks for the letters in the word but not in the rigth place
		for(int i = 0; i<tempColors.length;i++){
			if(tempGuess[i] != '0' && letterInWord(tempGuess[i], tempWord)){
				//if there is a letter in the word for a given character in the guess, 
				//it is removed from the word so that if there are two of a character in the guess (but one in the word)
				//it will only make one of them yellow
				tempWord[position] = '0';
				tempColors[i] = "Yellow";
			}
		}
		return tempColors;
	}
	
	//The ANSI color codes
	public static final String WHITE = "\u001B[37m";
	public static final String GREEN = "\u001B[32m";
	public static final String YELLOW = "\u001B[33m";
	
	public static void presentResult(String[] colors, String guess){
		int count = 0;
		System.out.println("");
		//prints one colored letter at a time
		for(int i = 0; i<colors.length; i++){
			switch(colors[i]){
				case "Green":
					//counts the number of exactly guessed letters
					count++;
					System.out.print(GREEN + guess.charAt(i) + WHITE);
					break;
				case "Yellow":
					System.out.print(YELLOW + guess.charAt(i) + WHITE);
					break;
				case "":
					System.out.print(WHITE + guess.charAt(i) + WHITE);
					break;
			}
		}
		System.out.println("");
		
		//If correctly guessed, set the gamestate to true ("the win codition")
		if(count == 5){
			gamestate = true;
		}
	}
}