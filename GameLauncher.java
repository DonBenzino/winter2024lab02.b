import java.util.Scanner;
public class GameLauncher{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		char choice = '1';
		
		//The loop will continue to run so long as the user inputs a valid number. This will also make it so that after a game it asks them again to input a number, thus letting them switch game or exit
		while(choice == '1' || choice == '2'){
			System.out.println("If you wish to play Hangman, type 1. If you wish to play Worlde, type 2. To exit, type something else");
			choice = reader.next().charAt(0);
			if(choice == '1'){
				HangMan.runGame();
			}
			if(choice == '2'){
				Wordle.runGame();
			}
		}
	}
}